package main

import (
	"fmt"

	"gitlab.com/josephburnett79/omg-rpc/example/omg/proto"
	"gitlab.com/josephburnett79/omg-rpc/pkg/client"
)

// We export omg as an import which we will bind before instantiating this module
//export omg
func omg(req *int32) **int32

// We choose "run()" as an entry point but it could be anything
// E.g. "event(*int32)" might provide an event to respond to or "_start" for main()
//export run
func run() {
	thing := "rpc"
	req := proto.OmgRequest{
		Thing: &thing,
	}
	reqBytes, err := req.MarshalVT()
	if err != nil {
		panic(err)
	}
	// We need to use the common calling convention to turn a byte slice into a pair of pointers
	p := client.BytesOut(reqBytes)
	// Just making an RPC here, nothing special
	r := omg(p)
	// And we use the inverse convention to get back a byte slice
	// This is a workaround for lacking interface types to pass complex types
	// Tinygo has its own convention for passing more than three arguments
	// It places the extras in a struct and passes a pointer to it
	// But to maximize our compatability we use our own convention
	// So we can run tinygo, rust, golang or whatever language / compiler WASM files
	// They just have to import our client.BytesIn and BytesOut functions
	resBytes := client.BytesIn(r)
	res := &proto.OmgResponse{}
	err = res.UnmarshalVT(resBytes)
	if err != nil {
		panic(err)
	}
	// WASI-enabled functions can connect to STDIN, STDOUT and STDERR
	// So we see logs from our function along with the vm logs (good for debugging)
	fmt.Println(*res.Message)
}

// We have to provide main, even if we don't use it
func main() {}
