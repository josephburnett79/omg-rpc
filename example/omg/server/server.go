package main

import (
	"os"

	"github.com/second-state/WasmEdge-go/wasmedge"
	"gitlab.com/josephburnett79/omg-rpc/example/omg/proto"
	"gitlab.com/josephburnett79/omg-rpc/pkg/service"
)

func omgService(reqBytes []byte) (resBytes []byte, err error) {
	// This service is a gRPC service prepending "omg" to things
	req := &proto.OmgRequest{}
	// We use vtprotobuf which provides serialization without reflection
	// Because we need tinygo for WASI (golang is working on it)
	// And tinygo doesn't support reflection (too big I guess)
	err = req.UnmarshalVT(reqBytes)
	if err != nil {
		return nil, err
	}
	message := "omg " + *req.Thing
	res := &proto.OmgResponse{
		Message: &message,
	}
	return res.MarshalVT()
}

func main() {
	// Set log level for the engine
	wasmedge.SetLogErrorLevel()
	// We are running WASI-enable guest functions
	conf := wasmedge.NewConfigure(wasmedge.WASI)
	vm := wasmedge.NewVMWithConfig(conf)
	wasi := vm.GetImportModule(wasmedge.WASI)
	wasi.InitWasi(
		// We can pass arguments to our function
		os.Args[1:],
		// The function can inherit our environment
		os.Environ(),
		// We can provide access to files and directories
		[]string{},
		// But we don't for now because are going to inject a host function for IO
	)
	// Read the function and validate it
	// But don't start yet because it doesn't have everything it needs
	err := vm.LoadWasmFile(os.Args[1])
	if err != nil {
		panic(err)
	}
	err = vm.Validate()
	if err != nil {
		panic(err)
	}
	// It needs to have a module providing the "omg" host function
	service.Register(vm, "omg", omgService)
	// Now we can start the VM
	err = vm.Instantiate()
	if err != nil {
		panic(err)
	}
	// And run our entry point function
	// The function can make calls to our "omg" service to do stuff
	// And then return something here (which we ignore for now)
	_, err = vm.Execute("run")
	if err != nil {
		panic(err)
	}
}
