.PHONY: example-protogen
example-protogen:
	cd example/omg;                                                                         \
	protoc                                                                                  \
	--go_out=. --plugin protoc-gen-go="${GOPATH}/bin/protoc-gen-go"                         \
	--go-grpc_out=. --plugin protoc-gen-go-grpc="${GOPATH}/bin/protoc-gen-go-grpc"          \
	--go-vtproto_out=. --plugin protoc-gen-go-vtproto="${GOPATH}/bin/protoc-gen-go-vtproto" \
	--go-vtproto_opt=features=marshal+unmarshal+size                                        \
	proto/omg.proto

.PHONY: example-build-client
example-build-client:
	cd example/omg;                                               \
	tinygo build -target=wasi -o omg-client.wasm client/client.go

.PHONY: example-build-server
example-build-server:
	cd example/omg;                          \
	go build -o omg-server server/server.go; 

.PHONY: example-build
example-build : example-build-server example-build-client

.PHONY: example-run
example-run:
	cd example/omg; \
	./omg-server omg-client.wasm

