package server

import (
	"fmt"
	"unsafe"

	"github.com/second-state/WasmEdge-go/wasmedge"
)

func BytesOut(vm *wasmedge.VM, b []byte) (int32, error) {
	// We will use the guest memory for returning a byte slice
	mem := vm.GetActiveModule().FindMemory("memory")
	if mem == nil {
		return 0, fmt.Errorf("memory not exported")
	}
	// Make space for the outgoing bytes
	g, err := vm.Execute(("malloc"), int32(len(b)))
	if err != nil {
		return 0, err
	}
	// Return value of malloc is the offset in guest linear memory
	bytesOffset := g[0].(int32)
	// Get a host byte slice for the guest byte slice
	guestBytes, err := mem.GetData(uint(bytesOffset), uint(len(b)))
	if err != nil {
		return 0, err
	}
	copy(guestBytes, b)
	// Make a guest int32 in which to store the byte slice length
	l, err := vm.Execute("malloc", int32(4))
	if err != nil {
		return 0, err
	}
	lengthOffset := l[0].(int32)
	// Get a host byte slice for the int32
	ls, err := mem.GetData(uint(lengthOffset), 4)
	if err != nil {
		return 0, err
	}
	// Get a pointer to the int32 so we can set it
	length := (*int32)(unsafe.Pointer(&ls[0]))
	*length = int32(len(b))
	// Make a guest slice of two *int32's
	// This is our calling convention for passing []byte
	// The first pointer is the start of the byte slice
	// The second pointer is the length of the byte slice
	// TODO: try stuffing all this into a single i64
	p, err := vm.Execute("malloc", int32(2*4)) // wasm pointers are 32 bits
	if err != nil {
		return 0, err
	}
	pointersOffset := p[0].(int32)
	// Get a host byte slice for the guest pointer slice
	ps, err := mem.GetData(uint(pointersOffset), uint(2*4))
	if err != nil {
		return 0, err
	}
	// Convert the byte slice (8) into an int32 slice (2)
	guestPointers := unsafe.Slice((*int32)(unsafe.Pointer(&ps[0])), 2)
	// Point the first int32 to the guest byte slice
	guestPointers[0] = bytesOffset
	guestPointers[1] = lengthOffset
	// Return the offset to the guest pointers
	return pointersOffset, nil
}

func BytesIn(vm *wasmedge.VM, i int32) ([]byte, error) {
	// We will read from guest memory for the incoming byte slice
	mem := vm.GetActiveModule().FindMemory("memory")
	if mem == nil {
		return nil, fmt.Errorf("memory not exported")
	}
	// Get an *int32 slice of host memory
	// This is our calling convention
	// The first pointer is the data
	// The second pointer is to an int32 with byte length
	ps, err := mem.GetData(uint(i), 2)
	if err != nil {
		return nil, err
	}
	// Convert the byte slice (8) into an int32 slice (2) of offsets
	guestPointers := unsafe.Slice((*int32)(unsafe.Pointer(&ps[0])), 2)
	// Get the offset of the byte slice
	offset := guestPointers[0]
	// Get the length of the byte slice
	l, err := mem.GetData(uint(guestPointers[1]), 4)
	if err != nil {
		return nil, err
	}
	length := *(*int32)(unsafe.Pointer(&l[0]))
	// Get a byte slice in guest memory at offset
	return mem.GetData(uint(offset), uint(length))
}
