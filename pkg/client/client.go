package client

import "unsafe"

func BytesOut(b []byte) *int32 {
	// Provide the byte slice length as a *int32
	l := int32(len(b))
	// Package a pointer to the underlying bytes and the length in a two pointer slice
	p := []*int32{(*int32)(unsafe.Pointer(&b[0])), &l}
	// Return a pointer to the underlying bytes of the two pointer slice
	return (*int32)(unsafe.Pointer(&p[0]))
}

func BytesIn(i **int32) []byte {
	// Convert the input pointer to a two pointer slice
	p := unsafe.Slice(&(*i), 2)
	// Read the length
	l := *p[1]
	// Convert the bytes pointer back into a byte slice
	return unsafe.Slice((*byte)(unsafe.Pointer(p[0])), l)
}
