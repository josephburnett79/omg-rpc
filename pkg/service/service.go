package service

import (
	"fmt"

	"github.com/second-state/WasmEdge-go/wasmedge"
	"gitlab.com/josephburnett79/omg-rpc/pkg/server"
)

func Register(vm *wasmedge.VM, serviceName string, handler func([]byte) ([]byte, error)) error {
	// We are binding a host function to a guest import of (param i32) (result i32)
	funcType := wasmedge.NewFunctionType(
		[]wasmedge.ValType{wasmedge.ValType_I32},
		[]wasmedge.ValType{wasmedge.ValType_I32},
	)
	// The host function implements wasmedge.hostFunctionSignature
	service := func(data interface{}, callframe *wasmedge.CallingFrame, params []interface{}) ([]interface{}, wasmedge.Result) {
		// We grab the first i32 and extract a []byte of guest memory according to our calling convention
		reqOffset := params[0].(int32)
		reqBytes, err := server.BytesIn(vm, reqOffset)
		if err != nil {
			return []interface{}{err}, wasmedge.Result_Fail
		}
		// The registered handler does something and returns a []byte or an error
		resBytes, err := handler(reqBytes)
		if err != nil {
			return []interface{}{err}, wasmedge.Result_Fail
		}
		// We allocate guest memory for the return and copy in the bytes
		resOffset, err := server.BytesOut(vm, resBytes)
		if err != nil {
			return []interface{}{err}, wasmedge.Result_Fail
		}
		// The return value is a guest memory offset, just like the input
		return []interface{}{resOffset}, wasmedge.Result_Success
	}
	// We create a VM function with the signature and implementation
	serviceFunc := wasmedge.NewFunction(funcType, service, nil, 0)
	funcType.Release()
	// Add the function to a new module
	env := wasmedge.NewModule("env")
	if env == nil {
		fmt.Errorf("nil module")
	}
	env.AddFunction(serviceName, serviceFunc)
	// And register the new module with the VM
	// Now the (import "env" "service" ...) WASM bytecodes will find the host function
	err := vm.RegisterModule(env)
	if err != nil {
		return err
	}
	return nil
}
