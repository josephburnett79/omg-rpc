module gitlab.com/josephburnett79/omg-rpc

go 1.20

require (
	github.com/bytecodealliance/wasmtime-go v1.0.0 // indirect
	github.com/second-state/WasmEdge-go v0.11.2 // indirect
	google.golang.org/protobuf v1.30.0 // indirect
)
